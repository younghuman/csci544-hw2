#Command

``` python3 perceplearn.py [training_file] [model_file] -h [dev_file] ```

The training file needs to be processed as the format below:
CLASS FEATURE1 FEATURE2
....

```python3 percepclassify [model_file] ```

It takes input from stdin and the input should be in the format as:
FEATURE1 FEATURE2 ...


##1) Tested on the development data set, the accuracy of my POS tagger is
    0.944

##2) Overall Precision: 0.8162368112543963
- Overall    Recall: 0.640082739600092
- Overall    Fscore: 0.7175061187685172
- LOC Precision: 0.7169625246548323
- LOC    Recall: 0.7388211382113821
- LOC    Fscore: 0.7277277277277276
- MISC Precision: 0.73046875
- MISC    Recall: 0.4202247191011236
- MISC    Fscore: 0.5335235378031383
- ORG Precision: 0.8559256390395042
- ORG    Recall: 0.65
- ORG    Fscore: 0.7388833166165162
- PER Precision: 0.900117508813161
- PER    Recall: 0.6268412438625205
- PER    Fscore: 0.7390255668113845


##3) Here is the metrics for Spam dataset with perceptron classifier:
- HAM Precision: 0.9859719438877755
- HAM Recall: 0.984
- HAM Fscore: 0.9849849849849849
- SPAM Precision: 0.9561643835616438
- SPAM Recall: 0.9614325068870524
- SPAM Fscore: 0.9587912087912088

###Here is the metrics for Spam dataset with Naive Beyes classifier:
- SPAM Precision: 0.9567567567567568
- SPAM Recall: 0.9752066115702479
- SPAM F Score: 0.965893587994543
- HAM Precision: 0.9909365558912386
- HAM Recall: 0.984
- HAM F Score: 0.9874560963371801

###Here is the metrics for Sentiment dataset with perceptron classifier:
- NEG Precision: 0.8474903474903475
- NEG Recall: 0.8762475049900199
- NEG Fscore: 0.8616290480863591
- POS Precision: 0.871900826446281
- POS Recall: 0.8423153692614771
- POS Fscore: 0.8568527918781726

###Here is the metrics for Spam dataset with Naive Beyes classifier:
- POS Precision: 0.7911547911547911
- POS Recall: 0.6427145708582834
- POS F Score: 0.7092511013215859
- NEG Precision: 0.6991596638655462
- NEG Recall: 0.8303393213572854
- NEG F Score: 0.7591240875912408

There is a significant improvement for Sentiment dataset with perceptron classifier but not for Spam dataset.
The reason might be that for Spam dataset, the precision and recall are already good enough with the independent assumption for Naive Beyes classifier, but for Sentiment dataset, the independent assumption is wrong and perceptron models the linear combination of all the features thus has better performance.






