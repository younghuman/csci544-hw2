import sys;
import re;
import os;
import json;
import codecs;
import operator; 
file1 = open(sys.argv[1], 'r', errors='ignore');
model = json.load(file1);
weights  = model['average_weights'];
priority = model['priority'];
sys.stdin = codecs.getreader('utf8')(sys.stdin.detach(), errors='ignore')
for line in sys.stdin:
    line = line.rstrip('\n');
    line = re.sub('\s+$', '', line);
    line = re.sub('^\s+', '', line);
    words = re.split('\s+', line);
    for i in range(len(words)):
        if(i==len(words)-1):
            next_word='##end##';
            next_tag='END';
        else:
            m = words[i+1].split('/');
            if(len(m)!=3 and len(m)!=2):
                  [next_word, next_tag] = ['#','#'];
            else: [next_word, next_tag] = operator.itemgetter(*[0,1])(m);
        if(i==0):
            pre_tag = 'START';
            pre_word= '##start##'; 
            pre_ner = 'START';
        else:
            pre_ner = max_ne;
            m = words[i-1].split('/');
            if(len(m)!=3 and len(m)!=2): 
                  [pre_word, pre_tag] = ['#','#'];
            else: [pre_word, pre_tag] = operator.itemgetter(*[0,1])(m);
        if(i<=1):
            pre2_tag = 'START';
            pre2_word= '##start##';
        else:
            m = words[i-2].split('/');
            if(len(m)!=3 and len(m)!=2): 
                  [pre2_word, pre2_tag] = ['#','#'];
            else: [pre2_word, pre2_tag] = operator.itemgetter(*[0,1])(m);
        if(i<=2):
            pre3_tag = 'START';
            pre3_word= '##start##';
        else:
            m = words[i-3].split('/');
            if(len(m)!=3 and len(m)!=2): 
                  [pre3_word, pre3_tag] = ['#','#'];
            else: [pre3_word, pre3_tag] = operator.itemgetter(*[0,1])(m);
         
        if(i>=len(words)-3):
            next3_word='##end##';
            next3_tag='END';
        else:
            m = words[i+3].split('/');
            if(len(m)!=3 and len(m)!=2): 
                  [next3_word, next3_tag] = ['#','#'];
            else: [next3_word, next3_tag] = operator.itemgetter(*[0,1])(m);

        if(i>=len(words)-2):
            next2_word='##end##';
            next2_tag='END';
        else:
            m = words[i+2].split('/');
            if(len(m)!=3 and len(m)!=2): 
                  [next2_word, next2_tag] = ['#','#'];
            else: [next2_word, next2_tag] = operator.itemgetter(*[0,1])(m);
        m = words[i].split('/');
        if(len(m)!=3 and len(m)!=2): 
              [cur_word,cur_tag] = ['#','#'];
        else: [cur_word,cur_tag] = operator.itemgetter(*[0,1])(m);
        max_score = -float('inf');
        max_priority = -float('inf');
        score = {};
        for subject in weights:
            score[subject] = 0;
            if( ('cur_word:'+cur_word) in weights[subject]):
                 score[subject]+=weights[subject]['cur_word:'+cur_word];
            if( ('pre_word:'+pre_word) in weights[subject]):
                 score[subject]+=weights[subject]['pre_word:'+pre_word];
            if( ('next_word:'+next_word) in weights[subject]):
                 score[subject]+=weights[subject]['next_word:'+next_word];
            if( ('pre_tag:' +pre_tag) in weights[subject]):
                 score[subject]+=weights[subject]['pre_tag:'+pre_tag];
            if( ('cur_tag:' +cur_tag) in weights[subject]):
                 score[subject]+=weights[subject]['cur_tag:'+cur_tag];
            if( ('next_tag:' +next_tag) in weights[subject]):
                 score[subject]+=weights[subject]['next_tag:'+next_tag];
            if( ('next2_word:' +next2_word) in weights[subject]):
                 score[subject]+=weights[subject]['next2_word:'+next2_word];
            if( ('pre2_word:' +pre2_word) in weights[subject]):
                 score[subject]+=weights[subject]['pre2_word:'+pre2_word];
            if( ('next3_word:' +next3_word) in weights[subject]):
                 score[subject]+=weights[subject]['next3_word:'+next3_word];
            if( ('pre3_word:' +pre3_word) in weights[subject]):
                 score[subject]+=weights[subject]['pre3_word:'+pre3_word];
            if( ('pre_ner:' +pre_ner) in weights[subject]):
                 score[subject]+=weights[subject]['pre_ner:'+pre_ner];
        for subject in score:    
            if(score[subject]>max_score or (score[subject]==max_score and priority[subject]>max_priority)):
                 max_score = score[subject];
                 max_priority = priority[subject];
                 max_ne = subject;
        if(i!=len(words)-1): print(cur_word+'/'+cur_tag+'/'+max_ne+" ", end="");
        else:   print(cur_word+'/'+cur_tag+ '/' +max_ne);






