import sys;
import re;
import os;
from optparse import OptionParser;
parser = OptionParser(conflict_handler="resolve");
parser.add_option("-h", dest="filename",metavar="FILE");
(options, args) = parser.parse_args();
dev_data = None;
if(options.filename):
   dev_file = options.filename;
   file2 = open(dev_file, 'r', errors='ignore');
   out_file2 = open("dev_temp","w");
   
file1 = open(sys.argv[1],'r', errors='ignore');
model_file = sys.argv[2];
out_file1 = open("training_temp", 'w');
def formatFiles (orig_file, out_file): 
  for line in orig_file:
    line = re.sub('\s+$', '', line);
    
    pos_tags = re.split('\s+', line);
    for i in range(len(pos_tags)):
        words = pos_tags[i].split('/');
        if(len(words)!=3):
            [cur_word,cur_tag,cur_ner]=['#','#','#'];
        else:
            [cur_word,cur_tag,cur_ner]=words;
        if(i<=2): 
            [pre3_word,pre3_tag] = ['##start##','START'];
        else: 
            words = pos_tags[i-3].split('/');
            if(len(words)!=3):
                [pre3_word,pre3_tag, pre3_ner]=['#','#','#'];
            else:
                [pre3_word,pre3_tag, pre3_ner] = words
        if(i<=1): 
            [pre2_word,pre2_tag] = ['##start##','START'];
        else: 
            words = pos_tags[i-2].split('/');
            if(len(words)!=3):
                [pre2_word,pre2_tag, pre2_ner]=['#','#','#'];
            else:
                [pre2_word,pre2_tag, pre2_ner] = words
        if(i==0): 
            [pre_word,pre_tag,pre_ner] = ['##start##','START','START'];
        else: 
            words = pos_tags[i-1].split('/');
            if(len(words)!=3):
                [pre_word,pre_tag, pre_ner]=['#','#','#'];
            else:
                [pre_word,pre_tag, pre_ner] = words;
        if(i== (len(pos_tags)-1) ): 
            [next_word, next_tag] = ['##end##', 'END'];
        else:  
            words = pos_tags[i+1].split('/');
            if(len(words)!=3):
               [next_word,next_tag,next_ner]=['#','#','#'];
            else:
               [next_word,next_tag,next_ner]= words;
        if(i>= (len(pos_tags)-2) ): 
            [next2_word,next2_tag] = ['##end##','END'];
        else: 
            words = pos_tags[i+2].split('/');
            if(len(words)!=3):
               [next2_word,next2_tag, next2_ner]=['#','#','#'];
            else:
               [next2_word,next2_tag, next2_ner] = words
        if(i>= (len(pos_tags)-3) ): 
            [next3_word,next3_tag] = ['##end##','END'];
        else: 
            words = pos_tags[i+3].split('/');
            if(len(words)!=3):
               [next3_word,next3_tag, next3_ner]=['#','#','#'];
            else:
               [next3_word,next3_tag, next3_ner] = words
        out_file.write(cur_ner+" pre_ner:"+pre_ner +
                               " pre_tag:"+pre_tag  +" pre_word:"+pre_word+
                               " cur_tag:"+cur_tag+" cur_word:"+cur_word+
                               " next_tag:"+next_tag+" next_word:"+next_word +
                               " next2_word:"+ next2_word+" pre2_word:"+pre2_word+
                               " next3_word:"+next3_word+" pre3_word:"+pre3_word+"\n");
formatFiles (file1, out_file1);
if(file2): formatFiles (file2, out_file2);
command = "python3 ../perceplearn.py training_temp "+model_file;
if(file2): command+=" -h dev_temp";
os.system(command);




        
              
