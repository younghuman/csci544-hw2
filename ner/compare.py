import sys;
import re;
true_result = sys.argv[1];
tagged_result=sys.argv[2];
file1 = open(true_result, 'r', errors='ignore');
file2 = open(tagged_result, 'r', errors='ignore');
true_tags = [];
true_total = {};
tagged_total = {};
shared_total = {};
total_count=0;
accurate_count =0;
for line in file1:
    line = line.rstrip('\n');
    line = re.sub('\s+$', '', line);
    line = re.sub('^\s+', '', line);
    words = re.split('\s+', line);
    for i in range(len(words)):
       total_count +=1;
       tags = words[i].split('/');
       true_tags.append(tags[-1]);
       m=re.match(r'^B-(\w+)',tags[-1]); 
       if(m):
        ne = m.group(1);
        if(ne in true_total):true_total[ne]+=1;
        else: true_total[ne] = 1;
i=0;
flag=0;
current_stat=0;
for line in file2:
    line = line.rstrip('\n');
    line = re.sub('\s+$', '', line);
    line = re.sub('^\s+', '', line);
    words = re.split('\s+', line);
    for each in words:
        tags = each.split('/');
        m=re.match(r'^B-(\w+)',tags[-1]); 
        if(m):
         ne = m.group(1);
         if(ne in tagged_total):tagged_total[ne]+=1;
         else:tagged_total[ne]=1;
        if(tags[-1]==true_tags[i] and tags[-1]!='O'):  
           [n1, n2] = tags[-1].split('-');
           [n1t,n2t] = true_tags[i].split('-'); 
           if(n1=='B'):
              if(flag==1 and current_stat!=0):
                  if(current_stat in shared_total):
                     shared_total[current_stat]+=1;
                  else:
                     shared_total[current_stat]=1;
              flag=1;  
              current_stat = n2;
        else:
           if(flag==1):
              flag=0;
              if(tags[-1]!='B-'+current_stat and true_tags[i]!='B-'+current_stat):
                  if(current_stat in shared_total):
                     shared_total[current_stat]+=1;
                  else:
                     shared_total[current_stat]=1;
        i += 1;
true = sum(true_total.values());
tagged=sum(tagged_total.values());
shared=sum(shared_total.values());
precision=shared/tagged;
recall   =shared/true;
fscore = 2*precision*recall/(precision+recall);
print("Overall Precision: "+ str(precision));
print("Overall    Recall: "+ str(recall));
print("Overall    Fscore: "+ str(fscore));
for tag in true_total:
    if(tag not in shared_total):shared_total[tag]=0;
    precision = float(shared_total[tag])/tagged_total[tag];
    recall = float(shared_total[tag])/true_total[tag];
    fscore = 2*precision*recall/(precision+recall);
    print(tag+" Precision: "+ str(precision));
    print(tag+"    Recall: "+ str(recall));
    print(tag+"    Fscore: "+ str(fscore));













