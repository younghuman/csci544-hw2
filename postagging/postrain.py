import sys;
import re;
import os;
from optparse import OptionParser;
parser = OptionParser(conflict_handler="resolve");
parser.add_option("-h", dest="filename",metavar="FILE");
(options, args) = parser.parse_args();
dev_data = None;
if(options.filename):
   dev_file = options.filename;
   file2 = open(dev_file, 'r');
   out_file2 = open("dev_temp","w");
   
file1 = open(sys.argv[1],'r');
model_file = sys.argv[2];
out_file1 = open("training_temp", 'w');
def formatFiles (orig_file, out_file): 
  for line in orig_file:
    line = re.sub('\s+$', '', line);
    pos_tags = re.split('\s+', line);
    for i in range(len(pos_tags)):
        [cur_word,cur_tag]=pos_tags[i].split('/');
        if(i==0): 
            [pre_word,pre_tag] = ['##start##','START'];
        else:  
            [pre_word,pre_tag] = pos_tags[i-1].split('/');
        if(i<=1):
            [pre_word_2, pre_tag_2] = ['##start##','START'];
        else:
            [pre_word_2, pre_tag_2] = pos_tags[i-2].split('/');
        if(i== (len(pos_tags)-1) ): 
            next_word = '##end##';
        else:  
            [next_word,next_tag]=pos_tags[i+1].split('/');
        out_file.write(cur_tag+" pre_tag:"+pre_tag  +" pre_word:"+pre_word+
                               " cur_word:"+cur_word+" next_word:"+next_word+
                               "\n");
formatFiles (file1, out_file1);
if(file2): formatFiles (file2, out_file2);
command = "python3 ../perceplearn.py training_temp "+model_file;
if(file2): command+=" -h dev_temp";
os.system(command);




        
              
