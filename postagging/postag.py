import sys;
import re;
import os;
import json;
file1 = open(sys.argv[1], 'r');
model = json.load(file1);
weights  = model['average_weights'];
priority = model['priority'];
for line in sys.stdin:
    line = line.rstrip('\n');
    line = re.sub('\s+$', '', line);
    line = re.sub('^\s+', '', line);
    words = re.split('\s+', line);
    pre_tag = 'START';
    pre_word= '##start##';
    for i in range(len(words)):
        if(i==len(words)-1):next_word='##end##';
        else:
            tags= words[i+1].split('/');
            next_word =  tags[0];
        if(i!=0):
            tags= words[i-1].split('/');
            pre_word =  tags[0];
            pre_tag = max_tag;
        tags=words[i].split('/');
        cur_word=tags[0];
        max_score = -float('inf');
        max_priority = -float('inf');
        score = {};
        for subject in weights:
            score[subject] = 0;
            if( ('cur_word:'+cur_word) in weights[subject]):
                 score[subject]+=weights[subject]['cur_word:'+cur_word];
            if( ('pre_word:'+pre_word) in weights[subject]):
                 score[subject]+=weights[subject]['pre_word:'+pre_word];
            if( ('next_word:'+next_word) in weights[subject]):
                 score[subject]+=weights[subject]['next_word:'+next_word];
            if( ('pre_tag:' +pre_tag) in weights[subject]):
                 score[subject]+=weights[subject]['pre_tag:'+pre_tag];
            if(score[subject]>max_score or (score[subject]==max_score and priority[subject]>max_priority)):
                 max_score = score[subject];
                 max_priority = priority[subject];
                 max_tag = subject;
        if(i!=len(words)-1): print(cur_word+'/'+max_tag+" ", end="");
        else:   print(cur_word+'/'+max_tag);






