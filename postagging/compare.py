import sys;
import re;
true_result = sys.argv[1];
tagged_result=sys.argv[2];
file1 = open(true_result, 'r');
file2 = open(tagged_result, 'r');
true_tags = [];
tagged_tags=[];
total_count=0;
accurate_count =0;
for line in file1:
    line = line.rstrip('\n');
    line = re.sub('\s+$', '', line);
    line = re.sub('^\s+', '', line);
    words = re.split('\s+', line);
    for each in words:
       total_count +=1;
       tags = each.split('/');
       true_tags.append(tags[1]);
i=0;
for line in file2:
    line = line.rstrip('\n');
    line = re.sub('\s+$', '', line);
    line = re.sub('^\s+', '', line);
    words = re.split('\s+', line);
    for each in words:
        tags = each.split('/');
        if(tags[1]==true_tags[i]):
           accurate_count += 1;
        i += 1;
print( 'Accuracy: '+ str(float(accurate_count)/total_count) );

