import sys;
import re;
import json;
if (len(sys.argv)!=2):
   sys.exit('Error: you have to give both the training file and model file!');
model_file = sys.argv[1];
file1 = open(model_file, 'r');
model = json.load(file1);
weights  = model['average_weights'];
priority = model['priority'];
for line in sys.stdin:
  line=line.rstrip("\n");
  reGroup = re.match("^(\w+) subject: (.*)$",line, re.I);
  if reGroup:
   subject = reGroup.group(1);
   content = reGroup.group(2);
  else: 
   [subject, content] = re.split("\s+",line,maxsplit=1);
  words = re.split("\s+",content);
  score = {};
  for word in words:
    for each_sub in weights:
        if each_sub not in score:
           score[each_sub] = 0;  
        if(word in weights[each_sub]):
           score[each_sub]+=weights[each_sub][word];
  max_score = -float("inf");
  for each_sub in score:
    if(score[each_sub]>max_score): max_score = score[each_sub];
  assigned_class = None;
  for each_sub in score:
      if(score[each_sub]==max_score):
            if(assigned_class is None): assigned_class=each_sub;
            else:
               if(priority[each_sub]>priority[assigned_class]):
                  assigned_class=each_sub;
  print(assigned_class);







