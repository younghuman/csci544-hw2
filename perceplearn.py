import sys;
import re;
import random;
import json;
import os;
from optparse import OptionParser;
path = os.path.dirname(os.path.abspath(__file__))
parser = OptionParser(conflict_handler="resolve");
parser.add_option("-h", dest="filename",metavar="FILE");
(options, args) = parser.parse_args();
dev_data = None;
if(options.filename):
   dev_file = options.filename;
   file3 = open(dev_file, 'r');
   dev_data = file3.readlines(); 
training_file = sys.argv[1];
model_file = sys.argv[2];
file1 = open(training_file, 'r');
file2 = open(model_file, 'w');
weights = {};              
averaged_weights = {};
out_weights = {};
priority = {};
word_list={};
training_list = [];
old_error = float("inf");error = 0;
old_dev_error=float("inf");
min_error = float("inf");
total = 0;current_step=1;

def check_dev(data, ave_weights):
 dev_count=0;
 error=0; 
 for line in data:
  dev_count+=1;
  line=line.rstrip("\n");
  reGroup = re.match("^(\w+) subject: (.*)$",line, re.I);
  if reGroup:
   subject = reGroup.group(1);
   content = reGroup.group(2);
  else: 
   [subject, content] = re.split("\s+",line,maxsplit=1);
  words = re.split("\s+",content);
  score = {};
  for word in words:
    for each_sub in ave_weights:
        if each_sub not in score:
           score[each_sub] = 0;  
        if(word in ave_weights[each_sub]):
           score[each_sub]+=ave_weights[each_sub][word];
  max_score = -float("inf");
  for each_sub in score:
    if(score[each_sub]>max_score): max_score = score[each_sub];
  assigned_class = None;
  for each_sub in score:
      if(score[each_sub]==max_score):
            if(assigned_class is None): assigned_class=each_sub;
            else:
               if(priority[each_sub]>priority[assigned_class]):
                  assigned_class=each_sub;
  if(assigned_class!=subject):error+=1;
 print("Dev error: "+str(float(error)/dev_count));
 return float(error/dev_count);

for line in file1:
   line=line.rstrip("\n");
   reGroup = re.match("^(\w+) subject: (.*)$",line, re.I);
   if(reGroup):
      subject = reGroup.group(1);
      content = reGroup.group(2);
   else:
      [subject, content] = re.split("\s+",line,maxsplit=1);
   
   weights[subject] = {};
   averaged_weights[subject] = {};
   training_list.append([subject, content]);
   words=re.split("\s+",content);
   total+=1;
i=0;
for subject in weights:
    i+=1;
    priority[subject] = i;
random.shuffle(training_list);
i=0;
while(i<len(training_list)):
   if(i==0):
       for each_sub in weights:
                  for word in weights[each_sub]:
                      averaged_weights[each_sub][word]+=weights[each_sub][word]*(len(training_list)+1);
   if_update_weights=0;
   [subject, content] = training_list[i];
   words = re.split("\s+",content);
   score = {};
   count = {};
   for word in words:
       if(word in count): count[word]+=1;
       else: count[word]=1;      
   for each_sub in weights:  
      for word in count:
           if(word not in weights[each_sub]): weights[each_sub][word]=0;
           if(word not in averaged_weights[each_sub]): averaged_weights[each_sub][word]=0;
           if(each_sub in score): 
              score[each_sub]+=weights[each_sub][word]*count[word];
           else:
              score[each_sub]=weights[each_sub][word]*count[word];
   max_sub=None;
   for each_sub in score:
       if(max_sub is None): max_sub=each_sub;
       elif(score[each_sub]>score[max_sub] or (score[each_sub]==score[max_sub] and priority[each_sub]>priority[max_sub])):
          max_sub=each_sub;
   if(max_sub!=subject):if_update_weights=1;

   if(if_update_weights):                
        error+=1;
        for word in count:        
                    weights[max_sub][word]-=count[word];
                    averaged_weights[max_sub][word]-=(len(training_list)-i)*count[word];
                    weights[subject][word]+=count[word]; 
                    averaged_weights[subject][word]+=(len(training_list)-i)*count[word];
   if(i==len(training_list)-1): 
             i=0;
             old_error=error;
             print("Current training step is "+str(current_step));
             print("Training error is "+"%0.6f"%(float(error)/total));   
             if(dev_data is not None):
               dev_error=check_dev(dev_data,averaged_weights);
               if(dev_error<min_error): 
                    min_error = dev_error;
                    out_weights = averaged_weights.copy();
               #if(dev_error>=old_dev_error):
               #     break;
               #else:
               #     old_dev_error = dev_error;
             error=0;
             current_step+=1;
             random.shuffle(training_list);
             if(current_step>10): break;
             continue;
   i+=1;
output={'average_weights':out_weights, 'priority':priority};
file2.write(json.dumps(output));





